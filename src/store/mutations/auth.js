export default {
  SET_TOKEN(state, data) {
    state.token = data
  },
  SET_REFRESH_TOKEN(state, data) {
    state.refreshToken = data
  },
  SET_USER(state, data) {
    state.user = data
  },
}
