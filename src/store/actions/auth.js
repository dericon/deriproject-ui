import axios from 'axios'
import router from '../../router'

export default {
  fetchToken(context, {username, password}) {
    const url = '[duse]/auth/token?includeRefreshToken=true';
    const promise = axios.get(url, {auth: {username, password}});

    promise.then(response => {
      context.commit('SET_TOKEN', response.data.token);
      context.commit('SET_REFRESH_TOKEN', response.data.refreshToken);
      router.push({path: '/'})
    }).catch(() => {
      context.dispatch('showSnackbar', {text: 'Sign in failed', type: 'error'})
    });

    return promise
  },
  refreshToken(context) {
    const url = `[duse]/auth/token/refresh?refreshtoken=${context.state.refreshToken}`;
    const promise = axios.get(url);

    return promise.then(response => {
      context.commit('SET_TOKEN', response.data.token)
    }, () => {
      context.dispatch('logout')
    })
  },
  fetchUser(context) {
    const promise = axios.get('[duse]/auth/token/me');

    promise.then(response => {
      context.commit('SET_USER', response.data)
    })
  },
  setUser(context, user) {
    context.commit('SET_USER', user)
  },
  logout(context) {
    context.commit('SET_TOKEN', null);
    context.commit('SET_REFRESH_TOKEN', null);
    router.push({path: '/login'})
  },
  breakToken(context) {
    context.commit('SET_TOKEN', '2iphbuei2bue2e');
    context.dispatch('showSnackbar', {text: 'Broke the token. Ouch!', type: 'warning'})
  },
  breakRefreshToken(context) {
    context.commit('SET_REFRESH_TOKEN', '2iphbuei2bue2e');
    context.dispatch('showSnackbar', {text: 'Broke the refresh token. Owiee!', type: 'warning'})
  }
}
