import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import mutations from './mutations'
import actions from './actions'
import getters from './getters'

Vue.use(Vuex);

export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    loaded: false,
    token: null,
    refreshToken: null,
    user: {
      name: null
    },
    roles: {
      data: {},
      order: []
    },
  },
  mutations,
  actions,
  getters
})
