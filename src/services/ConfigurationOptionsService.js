
import axios from 'axios'

export default {

    retrievePortRange: function() {
        return axios
            .get("[baseContextPath]/configuration-options/portRange")
            .then(response => {
                return response.data
            })
    },

    retrieveAvailableHosts: function() {
        return axios
            .get("[baseContextPath]/configuration-options/availableHosts")
            .then(response => {
                return response.data
            })
    },

    retrieveAvailableTemplates: function() {
        return axios
            .get("[baseContextPath]/configuration-options/availableTemplates")
            .then(response => {
                return response.data
            })
    },

    retrieveAvailableReviewers: function() {
        return axios
            .get("[baseContextPath]/configuration-options/availableReviewers")
            .then(response => {
                return response.data
            })
    },

    retrieveAvailableEnvironments: function() {
        return axios
            .get("[baseContextPath]/configuration-options/availableEnvironments")
            .then(response => {
                return response.data
            })
    },

    retrieveAvailableApplicationTypes: function() {
        return axios
            .get("[baseContextPath]/configuration-options/availableApplicationTypes")
            .then(response => {
                return response.data
            })
    }

}
