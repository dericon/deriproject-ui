
import axios from 'axios'

export default {
    getApplications: function () {
        return axios
            .get("[baseContextPath]/applications")
            .then(response => {
                return response.data;
            })
    },
    getApplicationsWithUnreleasedChanges: function () {
        return axios
            .get("[baseContextPath]/applications/containingUnreleasedChanges")
            .then(response => {
                return response.data;
            })
    },
    saveApplicationConfiguration: function (configuration) {
        return axios
            .post("[baseContextPath]/applications", configuration)
            .then(response => {
                return response.data;
            })
    }
}
