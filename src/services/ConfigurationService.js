
import axios from 'axios'

export default {

    submitConfiguration: function(requestDTO) {
        if (requestDTO.id === "") {
            return axios
                .post("[baseContextPath]/applications/initialize/configure", requestDTO)
                .then(response => {
                    return response.data;
                })
        } else {
            return axios
                .put("[baseContextPath]/applications/initialize/configure/" + requestDTO.id, requestDTO)
                .then(response => {
                    return response.data;
                })
        }
    },

    deleteConfiguration: function(id) {
        return axios
            .delete("[baseContextPath]/applications/initialize/configure/" + id)
    },

    getConfigurations: function () {
        return axios
            .get("[baseContextPath]/applications/initialize/configure")
            .then(response => {
                return response.data;
            })
    },

    getConfiguration: function (id) {
        return axios
            .get("[baseContextPath]/applications/initialize/review/" + id)
            .then(response => {
                return response.data;
            })
    }

}
