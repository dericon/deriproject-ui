import axios from 'axios'
import * as urlParse from 'url-parse'
import store from '../store/store.js'

// Auth Interceptor
axios.interceptors.request.use(config => {
  const url = urlParse(config.url);
  console.log(url);
  if (
      ['cloud.derif.in', 'cloud.derifin.net'].includes(url.hostname) &&
      url.pathname.includes('/api/auth/token') &&
      !url.pathname.includes('/api/auth/token/me')
  ) {
    if (process.env.NODE_ENV === 'development') {
      console.log('Using no authentication')
    }
    return config
  } else {
    config.headers = {
      ...config.headers,
      'Authorization': 'Bearer ' + store.state.token,
      retry: true
    };
    return config
  }
}, error => {
  return Promise.reject(error)
});

// Url Interceptor
axios.interceptors.request.use(config => {
  config.url = config.url.replace('[duse]', process.env.VUE_APP_API_DUSE);
  config.url = config.url.replace('[baseContextPath]', process.env.VUE_APP_BASE_CONTEXT_PATH);

  return config
}, error => {
  return Promise.reject(error)
});

axios.interceptors.response.use(config => {
  return config
}, error => {
  if (error.response) {
    console.log(error.config);
    if (error.config && error.config.headers.retry === true && (error.response.status === 401 || error.response.status === 403)) {
      return store.dispatch('refreshToken').then(token => {
        if (store.state.token) {
          error.config.headers = {
            ...error.config.headers,
            retry: false
          };
          return axios.request(error.config)
        }
      }, error => {
        console.log(error)
      })
    } else {
      return Promise.reject(error)
    }
  } else if (error.request) {
    console.log(error.request)
  } else {
    console.log(error.message)
  }
});
