import Vue from 'vue'
import App from './App.vue'
import store from './store/store.js'
import './helpers/interceptors'
import router from './router'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  components: {App},
  vuetify,
  render: h => h(App)
}).$mount('#app');
