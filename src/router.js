import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store/store.js'
import Dashboard from './views/Dashboard.vue'
import Initializer from './views/Initializer.vue'
import Login from './views/Login.vue'
import Logout from './views/Logout.vue'

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/dashboard'
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/initializer/:configurationId?',
      name: 'initializer',
      component: Initializer,
      props: true
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout
    }
  ]
});

router.beforeEach((to, from, next) => {
  const isLoggedIn = store.getters.isLoggedIn;

  setTimeout(() => {
    if ((isLoggedIn && to.fullPath !== '/login') || (!isLoggedIn && to.fullPath === '/login')) {
      next()
    } else if ((isLoggedIn && to.fullPath === '/login')) {
      next({
        path: '/dashboard'
      })
    } else {
      next({
        path: '/login'
      })
    }
  }, 200)
});

export default router
