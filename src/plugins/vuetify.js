import '@mdi/font/css/materialdesignicons.css'
import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'mdi'
    },
    theme: {
        themes: {
            light: {
                primary: '#42b983',
                secondary: '#9c9c9c',
                error: '#b00020',
                warning: '#e7a34d'
            }
        }
    }
});
